package com.example.studicaseibnu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView textView, textView2;
    private EditText input1, input2;
    private Button cek;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input1 = (EditText) findViewById(R.id.input1);
        input2 = (EditText) findViewById(R.id.input2);
        textView2 = (TextView) findViewById(R.id.textView2);
    }

    public void onClick(View view) {
        String alas = input1.getText().toString();
        String tinggi = input2.getText().toString();

        double a = Double.parseDouble(alas);
        double t = Double.parseDouble(tinggi);
        double luas = a * t;
        textView2.setText(" "+luas);
    }
}
